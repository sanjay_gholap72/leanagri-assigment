package com.sanjay.assigmentleanagri;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DataAdapter.MovieNoteListener
{
    private ProgressDialog mProgressDialog;
    private String url = "https://www.themoviedb.org/movie/";
    private ArrayList<String> mAuthorNameList = new ArrayList<>();
    private ArrayList<String> mBlogUploadDateList = new ArrayList<>();
    private ArrayList<String> mBlogTitleList = new ArrayList<>();
    private ArrayList<String> mBlogID =new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Description().execute();

    }

    @Override
    public void movieNoteClick(int position)
    {
        String MovieID = mBlogID.get(position);
        Intent intent = new Intent(MainActivity.this,ViewMovieActivity.class);
        intent.putExtra("Movie",MovieID);
        startActivity(intent);
    }


    private class Description extends AsyncTask<Void, Void, Void> implements DataAdapter.MovieNoteListener {
        String desc;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setTitle("LeanAgri");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            try {
                // Connect to the web site
                Document mBlogDocument = Jsoup.connect(url).get();
                // Using Elements to get the Meta data
                Elements mElementDataSize = mBlogDocument.select("div[class=flex]");
                // Locate the content attribute
                int mElementSize = mElementDataSize.size();

                for (int i = 0; i < mElementSize; i++)
                {

                    Elements mElementMovieTitle = mBlogDocument.select("a[class=title result]").eq(i);
                    String eMovieTitle = mElementMovieTitle.text();

                    Elements mElementMovieOverView = mBlogDocument.select("p[class=overview]").eq(i);
                    String eMovieOverView = mElementMovieOverView.text();

                    Elements images = mBlogDocument.select("img[class=poster lazyload fade]").eq(i);
                    String imgSrcStr = images.attr("data-src");

                    Elements mElementMovieID = mBlogDocument.select("a[class=result]").eq(i);
                    String mMovieID = mElementMovieID.attr("href");

                    Log.wtf("mMovieID",mMovieID);

                    mAuthorNameList.add(eMovieOverView);
                    mBlogUploadDateList.add(imgSrcStr);
                    mBlogTitleList.add(eMovieTitle);
                    mBlogID.add(mMovieID);
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            // Set description into TextView

            RecyclerView mRecyclerView = (RecyclerView)findViewById(R.id.act_recyclerview);

            DataAdapter mDataAdapter = new DataAdapter(MainActivity.this, mBlogTitleList, mAuthorNameList, mBlogUploadDateList,mBlogID,this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mDataAdapter);

            mProgressDialog.dismiss();
        }


        @Override
        public void movieNoteClick(int position)
        {
            String MovieID = mBlogID.get(position);
            Intent intent = new Intent(MainActivity.this,ViewMovieActivity.class);
            intent.putExtra("Movie",MovieID);
            startActivity(intent);
        }
    }
}