package com.sanjay.assigmentleanagri;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class ViewMovieActivity extends AppCompatActivity
{
    private String movieID;
    private String url = "https://www.themoviedb.org";
    private String finalURL;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_movie);

        Bundle bundle = getIntent().getExtras();
        movieID = bundle.getString("Movie");

        finalURL = url+movieID;

        Log.wtf("movieID",movieID);
        Log.wtf("URL",finalURL);
    }
}
