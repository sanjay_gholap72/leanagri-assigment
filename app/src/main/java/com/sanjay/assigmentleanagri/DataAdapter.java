package com.sanjay.assigmentleanagri;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import me.biubiubiu.justifytext.library.JustifyTextView;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {

    private ArrayList<String> mBlogTitleList = new ArrayList<>();
    private ArrayList<String> mAuthorNameList = new ArrayList<>();
    private ArrayList<String> mBlogUploadDateList = new ArrayList<>();
    private ArrayList<String> mBlogID = new ArrayList<>();
    private Activity mActivity;
    private int lastPosition = -1;
    private  MovieNoteListener movieNoteListener;

    public DataAdapter(MainActivity activity, ArrayList<String> mBlogTitleList, ArrayList<String> mAuthorNameList, ArrayList<String> mBlogUploadDateList,ArrayList<String> mBlogID,MovieNoteListener movieNoteListener)
    {
        this.mActivity = activity;
        this.mBlogTitleList = mBlogTitleList;
        this.mAuthorNameList = mAuthorNameList;
        this.mBlogUploadDateList = mBlogUploadDateList;
        this.mBlogID = mBlogID;
        this.movieNoteListener = movieNoteListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_blog_title;
        private ImageView imageView;
        private JustifyTextView tv_blog_author;
        MovieNoteListener movieNoteListener;


        public MyViewHolder(View view,MovieNoteListener movieNoteListener)
        {
            super(view);
            this.movieNoteListener = movieNoteListener;

            tv_blog_title = (TextView) view.findViewById(R.id.row_tv_blog_title);
            tv_blog_author = (JustifyTextView) view.findViewById(R.id.row_tv_blog_author);
            imageView = (ImageView) view.findViewById(R.id.view_imgcd);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
             movieNoteListener.movieNoteClick(getAdapterPosition());
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_data, parent, false);
        return new MyViewHolder(itemView,movieNoteListener);
    }

    @NonNull


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        holder.tv_blog_title.setText(mBlogTitleList.get(position));
        holder.tv_blog_author.setText(mAuthorNameList.get(position));

        String movieID = mBlogID.get(position);

        Glide.with(this.mActivity)
                .load(mBlogUploadDateList.get(position))
                .diskCacheStrategy(DiskCacheStrategy.NONE) // <= ADDED
                .skipMemoryCache(true) // <= ADDED
                .into(holder.imageView);


    }

    @Override
    public int getItemCount()
    {
        return mBlogTitleList.size();
    }

    public interface MovieNoteListener
    {
        void movieNoteClick(int position);
    }
}

